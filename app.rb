require 'sinatra'
require_relative 'services/calculator_service'

get '/' do
    erb :calculator
end

# param: formula
post '/calculate' do
    begin
        result = CalculatorService.calculate params[:formula]
    rescue => exception
        {:success => false, :message => exception}.to_json
    else
        {:success => true, :score => result}.to_json
    end
end

get '/contact' do
    erb :contact
end
