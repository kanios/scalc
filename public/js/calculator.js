$('#countBtn').click(() => {
    $.post('/calculate', {
        formula: $('#formula').val()
    }).done((response) => {
        response = JSON.parse(response);
        if (response.success) {
            $('#score').val(response.score);
            $('#calcError').addClass('hidden');
        } else {
            $('#calcError').text('Something went wrong. Error: ' + response.message)
            $('#calcError').removeClass('hidden');
        }
    });
});

$('#formula').on('keydown', event => {
    if (event.key === 'Enter') {
        $('#countBtn').click();
    }
});
