**SCalc**
-

**Description:**
--

*SCalc* is a science calculator that supports such mathematical functions as factorial, sinus, cosinus, parenthesis etc. and  
calculating non-trivial mathematical formulas using the RACC parser and regular expressions.

**Requirements:**
--

Application was tested on machine with installed:

- ruby v. 2.6.3p62
- bash shell

**Running application**:
--

**Quick method:**

Run bash script by putting `bash start.sh` into your console in main project's directory.

**Step by step method:**

- Install required gems by `bundle` command in main project's directory.
- By using RACC we compile parser code by typing `racc -o tools/calculator/calc tools/calculator/calc.y` in main project's directory.
- We grant essential permissions to our new _calc_ script by typing `chmod 777 tools/calculator/calc` in main project's directory.
- We run application by typing `ruby app.rb` in main project's directory.

Calculator is available under http://0.0.0.0/4567 in your browser or there is terminal version ready in `tools/calculator` directory, where we run _calc_ script by typing `ruby calc` in proper directory.

Testing:
--

Unit tests might be run by typing `rspec` in main project's directory. Implementation of tests you will find in `rspec/` directory.

Stack:
--

- Ruby
- RACC
- Sinatra
- Rspec
- HTML
- JQuery
- CSS
- GIT

---

Opis:
--

*SCalc* to kalkulator naukowy obsługujący takie matematyczne funkcje jak: oraz obliczający nietrywialne formuły matematyczne przy uzyciu parsera RACC oraz wyrazeń regularnych.

Wymagania:
--

Aplikacja była testowana na komputerze z zainstalowanymi:

- ruby v. 2.6.3p62
- powłoka bash

Uruchamianie aplikacji:
--

**Metoda szybka:**
Wykonujemy skrypt `start.sh` przy pomocy komendy `bash start.sh` w głównym katalogu projektu.

**Metoda _po kolei_:**

- Instalujemy wymagane gemy przy pomocy komendy `bundle` w głównym katalogu projektu.
- Korzystając z RACC kompilujemy nasz kod parsera wpisując `racc -o tools/calculator/calc tools/calculator/calc.y` w głównym katalogu projektu.
- Nadajemy odpowiednie uprawnienia do nowopowstałego pliku `calc`, wpisując `chmod 777 tools/calculator/calc` w głównym katalogu projektu.
- Uruchamiamy aplikację webową do obsługi kalkulatora przy pomocy `ruby app.rb` wykonanego w głównym katalogu projektu.

Kalkulator jest dostępny zarówno pod adresem http://0.0.0.0/4567 w twojej przeglądarce lub wersja terminalowa dostępna jest w katalogu `tools/calculator`, gdzie wykonujemy skrypt _calc_ wpisując `ruby calc` w odpowiednim katalogu.

Testowanie:
--

Testy jednostkowe uruchamiamy wykonując polecenie `rspec` w katalogu głównym projektu. Implementacje testów znajdziemy w katalogu `rspec/`. 

Technologie:
--

- Ruby
- RACC
- Sinatra
- Rspec
- HTML
- JQuery
- CSS
- GIT