class CalculatorService
    # formula : string
    def self.calculate(formula)
        file = File.open('tools/calculator/tmp', 'w')
        file.write(formula + "\n")
        file.close()
        
        `ruby tools/calculator/calc < tools/calculator/tmp`.to_f
    end
end
