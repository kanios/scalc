require_relative '../services/calculator_service'

describe CalculatorService do
    describe "scoring" do
        it "5+3-1+7-3! = 8" do
            expect(CalculatorService.calculate('5+3-1+7-3!')).to eql(8.0)
        end

        it "3*3-((2+3)*(2+3)*(2+3)*8-4*(6-1)) = -971" do
            expect(CalculatorService.calculate('3*3-((2+3)*(2+3)*(2+3)*8-4*(6-1))')).to eql(-971.0)
        end

        it "3^2-((2+3)^3*8-4*(6-2^0)) = -971" do
            expect(CalculatorService.calculate('3^2-((2+3)^3*8-4*(6-2^0))')).to eql(-971.0)
        end

        it "3+5 = 8" do
            expect(CalculatorService.calculate('3+5')).to eql(8.0)
        end

        it "5-3 = 2" do
            expect(CalculatorService.calculate('5-3')).to eql(2.0)
        end

        it "6/3 = 2" do
            expect(CalculatorService.calculate('6/3')).to eql(2.0)
        end

        it "9^0.5 = 3" do
            expect(CalculatorService.calculate('9^0.5')).to eql(3.0)
        end

        it "sin(1.0)^2 + cos(1.0)^2 = 1" do
            expect(CalculatorService.calculate('sin(1.0)^2 + cos(1.0)^2')).to eql(1.0)
        end
    end
end
