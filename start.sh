#!/bin/bash
echo "Installing gems..."
bundle
echo "Compiling calculator..."
racc -o tools/calculator/calc tools/calculator/calc.y
echo "Granting permissions..."
chmod 777 tools/calculator/calc
echo "Running rspec and starting app..."
 rspec && ruby app.rb
