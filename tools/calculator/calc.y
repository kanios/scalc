class Calcp
  prechigh
    nonassoc SIN COS TG COS LN
    nonassoc UMINUS
    right '!'
    left '^' 'ln'
    left '*' '/'
    left '+' '-'
  preclow
rule
  target: exp
        | /* none */ { result = 0 }

  exp: exp '+' exp          { result += val[2]      }
     | exp '-' exp          { result -= val[2]      }
     | exp '*' exp          { result *= val[2]      }
     | exp '/' exp          { result /= val[2]      }
     | exp '^' exp          { result **= val[2]     }
     | exp '!'              { result = fact(result) }
     | '(' exp ')'          { result = val[1]       }
     | '-' NUMBER  =UMINUS  { result = -val[1]      }
     | LN                   { result = ln(result)   }
     | SIN                  { result = sin(result)  }
     | COS                  { result = cos(result)  }
     | TG                   { result = tg(result)   }
     | CTG                  { result = ctg(result)  }
     | NUMBER

end

---- header
#
---- inner
  
  def parse(str)
    @q = []
    until str.empty?
      case str
      when /\A\d+(\.\d+)?/
        @q.push [:NUMBER, $&.to_f]
      when /\A\d+\!/
        @q.push [:NUMBER, $&.to_i]
      when /\Aln\(\d+(\.\d+)?\)/
        @q.push [:LN, $&]
      when /\Asin\(\d+(\.\d+)?\)/
        @q.push [:SIN, $&]
      when /\Acos\(\d+(\.\d+)?\)/
        @q.push [:COS, $&]
      when /\Atg\(\d+(\.\d+)?\)/
        @q.push [:TG, $&]
      when /\Actg\(\d+(\.\d+)?\)/
        @q.push [:CTG, $&]
      when /\A\s+/
      when /\A.|\n/o
        s = $&
        @q.push [s, s]
      end
      str = $'
    end
    @q.push [false, '$end']
    do_parse
  end

  def next_token
    @q.shift
  end

  def fact(str)
    result = 1
    for i in 1..str.to_i
      result *= i
    end
    result
  end

  def ln(str)
    Math.log(str.sub('ln(', '').sub(')', '').to_f)
  end

  def sin(str)
    Math.sin(str.sub('sin(', '').sub(')', '').to_f)
  end

  def cos(str)
    Math.cos(str.sub('cos(', '').sub(')', '').to_f)
  end

  def tg(str)
    Math.tan(str.sub('tg(', '').sub(')', '').to_f)
  end

  def ctg(str)
    1 / Math.tan(str.sub('ctg(', '').sub(')', '').to_f)
  end

---- footer

parser = Calcp.new
while true
  str = gets
  break if /q/i =~ str || str.nil?
  str = str.chop!
  begin
    puts "#{parser.parse(str)}"
  rescue ParseError
    puts $!
  end
end
